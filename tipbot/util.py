#!/usr/bin/env python3
# SPDX-License-Identifier: GPL2.0
# Copyright Thomas Gleixner <tglx@linutronix.de>

import traceback
import syslog
import sys

class FetchException(Exception):
    pass

class FatalException(Exception):
    pass

class logger(object):
    def __init__(self, use_syslog=False, verbose=False):
        self.use_syslog = use_syslog
        self.verbose = verbose
        self.warnings = ''
        self.exceptions = ''
        self.syslog_warn = syslog.LOG_MAIL | syslog.LOG_WARNING
        self.syslog_info = syslog.LOG_MAIL | syslog.LOG_INFO
        self.syslog_debug = syslog.LOG_MAIL | syslog.LOG_DEBUG

    def log_debug(self, txt):
        if self.verbose:
            if self.use_syslog:
                syslog.syslog(self.syslog_debug, txt)
            else:
                sys.stderr.write(txt)

    def log(self, txt):
        if self.use_syslog:
            syslog.syslog(self.syslog_info, txt)
        else:
            sys.stderr.write(txt)

    def log_warn(self, txt):
        self.warnings += txt
        if self.use_syslog:
            syslog.syslog(self.syslog_warn, txt)
        else:
            sys.stderr.write(txt)

    def log_exception(self, ex, msg=''):
        txt = 'tip-bot2: %s%s' %(msg, ex)
        if self.verbose:
            txt += '%s\n' % (traceback.format_exc())
        self.exceptions += txt
        self.log_warn(txt)
