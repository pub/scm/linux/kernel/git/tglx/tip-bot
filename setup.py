#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-only
# Copyright Thomas Gleixner <tglx@linutronix.de>

from glob import glob
from distutils.core import setup

from tipbot.version import __version__

setup(name='tipbot',
      version=__version__,
      description='tipbot',
      author='Thomas Gleixner',
      author_email='tglx@linutronix.de',
      packages=['tipbot'],
      scripts=['tipbot_daemon']
      )
data_files = [
    ('/lib/systemd/system',  glob("tipbot.service"))],
